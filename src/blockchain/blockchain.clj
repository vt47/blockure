(ns blockchain.blockchain
  (:require [clojure.walk :refer :all]
            [clj-time.core :as time]
            [clj-time.coerce :as time-coerce]
            [clj-http.client :as client]
            [pandect.algo.sha256 :refer :all])
  (:import (java.util UUID)))

;; Instantiation: constants & atoms
(def genesis-block {:index 0
                    :timestamp 123456789
                    :transactions []
                    :hash "9cb345d0ec9bfc248497286166505ed9f643aca499d86b74088000909226c91f"
                    :previous-hash 0
                    :proof 100})

(def chain (atom [genesis-block]))

(def nodes (atom []))

(def current-transactions (atom []))


;; Mechanism
(defn new-block [proof previous-hash]
  (let [made-block {:index (inc (count chain))
                    :timestamp (time-coerce/to-long (time/now))
                    :transactions current-transactions
                    :proof proof
                    :previous-hash previous-hash}]
    (do
      (swap! chain #(conj % made-block))
      (swap! current-transactions [])
      (made-block))))

(defn new-transaction [sender recipient amount]
  (swap!
   current-transactions
   #(conj % {:sender sender
             :recipient recipient
             :amount amount})))

(defn hash [block] nil)

(defn last-block [] nil)

(defn validate-proof [proof last-proof]
  (let [guess-hash (sha256 (str last-proof proof))]
    (= (subs guess-hash 0 4) "0000")))

(defn proof-of-work [last-proof]
  (loop [proof 0]
    (if-not (validate-proof proof last-proof)
      (recur (inc proof)))))
